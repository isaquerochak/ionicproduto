import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { Title } from '@angular/platform-browser/src/browser/title';
import { DatabaseProvider } from '../providers/database/database';
import { LoginPage } from '../pages/login/login';
import { SenhaCurtaProvider } from '../providers/senha-curta/senha-curta';
import { LoginCurtaPage } from '../pages/login-curta/login-curta';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage:any = LoginPage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen,
    public dbProvider: DatabaseProvider,
    public senhac: SenhaCurtaProvider ){
    
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();      

    });
    dbProvider.createDatabase()
      .then(() => {
        
      })
      .catch(() =>{

      });

    senhac.checkExistSenha()
    .then((ok)=>{
      
      if(ok == true){
        this.rootPage = LoginCurtaPage;
      }else{
        this.rootPage = LoginPage;
      }
    })
    .catch((err)=>{
      console.log('Erro'+err);
    });
    
    this.pages = [
      {title: 'Home', component: HomePage},
      {title: 'Categoria', component:'CategoriaPage'},
      {title: 'Produto', component: 'ProdutoPage'},
      {title: 'SenhaCurta', component: 'SenhaCurtaPage'},
      {title: 'Fechar', component: 'Fechar'}
    ];

  }

  openPage(page){
    if (page.title == "Fechar"){
      this.platform.exitApp();
    }
    else{
      this.nav.setRoot(page.component);
    }
  }
  
}

