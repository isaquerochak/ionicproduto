import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ProdutoProvider } from '../providers/produto/produto';
import { CategoriaProvider } from '../providers/categoria/categoria';
import { DatabaseProvider } from '../providers/database/database';
import {Camera, CameraOptions} from '@ionic-native/camera';

import { SQLite } from '@ionic-native/sqlite';
import { LoginPage } from '../pages/login/login';
import { LoginCurtaPage } from '../pages/login-curta/login-curta';

import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { File } from '@ionic-native/file';
import { SenhaCurtaProvider } from '../providers/senha-curta/senha-curta';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    LoginCurtaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    LoginCurtaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    SQLite,
    ProdutoProvider,
    CategoriaProvider,
    DatabaseProvider,
    Camera, 
    BarcodeScanner,
    File,
    SenhaCurtaProvider
    
  ]
})
export class AppModule {}
