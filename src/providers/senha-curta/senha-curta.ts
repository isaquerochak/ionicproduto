//import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { File } from '@ionic-native/file';

/*
  Generated class for the SenhaCurtaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SenhaCurtaProvider {
  conteudoArquivo: any;
  path = this.file.dataDirectory;
  arquivo = 'senhac.txt';

  constructor(
  public alertCtrl: AlertController,
  private file: File) {
    console.log('Hello SenhaCurtaProvider Provider');
  }
  getSenha(){
    return this.file.readAsText(this.path, this.arquivo)
    .then( (ok)=> {
      this.conteudoArquivo = JSON.stringify(ok);
      //this.alerta(this.conteudoArquivo,"Conteudo"); 
      //return this.conteudoArquivo;     
      return ok;     
    })
  /*  ,
    (err)=> {
      //this.alerta(err.code.toString() + " " + 
     // err.message.toString(), "Atenção" );
     // this.erroTela = JSON.stringify(err);
    })*/
    .catch ((err) =>{ 
      //this.alerta("erro leitura" + err, "Atenção")
      return err;
    });
  }
  checkExistSenha(){
    return this.file.checkFile(this.path,this.arquivo)
    .then((result)=>{
      return result;
    })
    .catch((err)=>{
      return err;
    });
  }
  alerta(mensagem : string, cabec : string){ 
   
    let alert = this.alertCtrl.create({
      title: cabec,
      subTitle: mensagem,
      buttons: ['OK']
    });
    alert.present();
  }

}
