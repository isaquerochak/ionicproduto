import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SenhaCurtaProvider} from "../../providers/senha-curta/senha-curta";
import { HomePage } from '../home/home';

/**
 * Generated class for the LoginCurtaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-curta',
  templateUrl: 'login-curta.html',
})
export class LoginCurtaPage {
  senha:String = '';
  senhatxt: any = '';
  constructor(public navCtrl: NavController, public navParams: NavParams,
  public senhac: SenhaCurtaProvider,
  public alertCtrl : AlertController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginCurtaPage');
  }

  logar(){
   this.senhac.getSenha()
   .then((result)=>{
     console.log(result);
     console.log(this.senha);
      this.senhatxt = result;

      if(this.senhatxt == this.senha){
        this.navCtrl.setRoot(HomePage);
      }else{
        this.showAlert('Erro',"Senha Inválida!");
        this.navCtrl.setRoot(LoginCurtaPage);
      }
   })
   .catch((err)=>{
    this.showAlert('Erro',err);
   });
  }

  showAlert(titulo, subTitle) {
  
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: subTitle,
      buttons: ['OK']
    });

    alert.present();

  }
}
