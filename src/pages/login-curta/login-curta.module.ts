import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginCurtaPage } from './login-curta';

@NgModule({
  declarations: [
    LoginCurtaPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginCurtaPage),
  ],
})
export class LoginCurtaPageModule {}
