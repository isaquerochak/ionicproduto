import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {



  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController) {

  }

  irSenha(){
    this.navCtrl.push('SenhaCurtaPage');
  }
 
  irPage(pagina: string){
    this.navCtrl.push(pagina);
  }
}