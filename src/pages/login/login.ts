import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  codUser:String = '';
  senha:String = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController) {
  }

  logar(){
    if(this.codUser.trim() == ''){
  
      this.showAlert('Login','Codigo do usuario não pode estar vazio!');
  
    }else if(this.senha.trim() == ''){
  
      this.showAlert('Login','Senha não pode estar vazia!');
  
    }else if (this.codUser == "admin" && this.senha == "123"){
  
        this.navCtrl.setRoot(HomePage);
  
      }else{
  
        this.showAlert('Login','Dados para login invalidos');
  
      }
    }
  
    showAlert(titulo, subTitle) {
  
      let alert = this.alertCtrl.create({
        title: titulo,
        subTitle: subTitle,
        buttons: ['OK']
      });
  
      alert.present();
  
    }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

}
