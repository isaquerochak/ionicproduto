import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { File } from '@ionic-native/file';

/**
 * Generated class for the SenhaCurtaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-senha-curta',
  templateUrl: 'senha-curta.html',
})
export class SenhaCurtaPage {
  erroTela : any = '';
  nativeURL : any = '';
  conteudoArquivo : any = '';
  path = this.file.dataDirectory;
  arquivo = 'senhac.txt';


  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public alertCtrl: AlertController, private file: File) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SenhaCurtaPage');
  }

  presentPrompt() {
    let alert = this.alertCtrl.create({
      title: 'Senha Curta',
      inputs: [
        {
          name: 'senha',
          placeholder: 'Digita sua Senha Curta'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked' + data.senha);
          }
        },
        {
          text: 'Salvar',
          handler: data => {
            console.log('clicked save ' + data.senha);
            this.salvaSenha(data.senha);
          }
        }
      ]
    });
    alert.present();
  }

  alerta(mensagem : string, cabec : string){ 
   
    let alert = this.alertCtrl.create({
      title: cabec,
      subTitle: mensagem,
      buttons: ['OK']
    });
    alert.present();
  }

  salvaSenha(senhatxt: any){

    //console.log(this.path);
    //this.alerta(this.path,'Status');

    this.file.createFile(this.path, this.arquivo, true)
    .then( (ok) => {              
      this.alerta('Senha Cadastrada','Status');
    },
    (err)=> {
      this.alerta(err.code.toString() + " " + 
      err.message.toString(), "Atenção" );
      this.alerta('Erro no procesasmento:' + JSON.stringify(err),'Status');
    })
    .catch((err) => {
      console.log(err);
      this.alerta("Erro Write" + err.message.toString(), "Atenção");
    });

    this.file.writeFile(this.path, this.arquivo, senhatxt, {replace: true})
    .then( (ok) => {
      //this.retorno2 = JSON.stringify(ok);
      this.nativeURL = ok.nativeURL;

      },
    (err)=> {
      this.alerta(err.code.toString() + " " + 
      err.message.toString(), "Atenção" );
      this.erroTela = JSON.stringify(err);
    })
    .catch((err) => {
      this.alerta("Erro Write" + err, "Atenção");
    });
  }

  lerTxt(){
    this.file.readAsText(this.path, this.arquivo)
    .then( (ok)=> {
        this.conteudoArquivo = JSON.stringify(ok);
        this.alerta(this.conteudoArquivo,"Conteudo");           
     }
    ,
    (err)=> {
      this.alerta(err.code.toString() + " " + 
      err.message.toString(), "Atenção" );
      this.erroTela = JSON.stringify(err);
    })
    .catch ((err) => this.alerta("erro leitura" + err, "Atenção"));
  }
  apagarTxt(){
    this.file.removeFile(this.path, this.arquivo)
    .then((ok)=>{
      this.alerta("Senha excluida com sucesso","Ok");  
    })
    .catch((err)=>{
      this.alerta("Erro na exclusão" + err, "Atenção");
    });
  }

}

