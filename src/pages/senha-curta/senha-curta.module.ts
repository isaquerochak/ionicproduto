import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SenhaCurtaPage } from './senha-curta';

@NgModule({
  declarations: [
    SenhaCurtaPage,
  ],
  imports: [
    IonicPageModule.forChild(SenhaCurtaPage),
  ],
})
export class SenhaCurtaPageModule {}
